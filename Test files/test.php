<?php

class FonctionsTest extends PHPUnit_Framework_TestCase

{

    protected $monPdotest;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     *
     * Constructeur du test
     */
    protected function setUp()
    {
        require_once("../include/class.pdogsb.inc.php");

        $this->monPdotest = PdoGsb::getPdoGsb();

        PdoGsb::getPdo()->exec("SET AUTOCOMMIT OFF;");
        PdoGsb::getPdo()->beginTransaction();

    }

    protected function tearDown()
    {
        PdoGsb::getPdo()->rollBack();
        PdoGsb::getPdo()->exec("SET AUTOCOMMIT ON;");
    }

    public function testerMoisSuivant()

    {

        include("../include/fct.inc.php");
        $leMois = incrementerMois(201601);
        $this->assertEquals($leMois, 201602);
    }


    public function testGetInfosVisiteur()
    {
        // Remove the following lines when you implement this test.

        $visiteur = array(

            'nom' => "Villechalane",
            'prenom' => "Louis"

        );


        $return = $this->monPdotest->getInfosVisiteur('a131');


        $this->assertEquals($visiteur, $return);


    }

    public function testmajFraisForfait()
    {
        $fiche = array(
            'ETP' => '2',
        );
        $this->monPdotest->majFraisForfait("a131", "201402", $fiche);

        $req = "SELECT lignefraisforfait.quantite
                FROM lignefraisforfait
                WHERE lignefraisforfait.idvisiteur = 'a131'
                AND lignefraisforfait.mois = '201402'
                AND lignefraisforfait.idfraisforfait = 'ETP'";
        $res = PdoGsb::getPdo()->query($req);
        $etat = $res->fetch();

        $qte = array(
            'quantite' => '2',
            0 => '2');

        $this->assertEquals($qte, $etat);
    }

}


?>
