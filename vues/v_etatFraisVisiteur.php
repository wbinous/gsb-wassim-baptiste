<h3>Fiche de frais du mois <?php echo $numMois."-".$numAnnee?> : </h3>

<div class="encadre">
    <p class="p">Etat : <?php echo $libEtat ?> depuis le <?php echo $dateModif?> <br></p>
        <!-- tableau des frais forfait -->
  	<table class="suivi">
            <!-- entete du tableau -->
            <tr>
                <th></th>
        <?php
        foreach ( $lesFraisForfait as $FraisForfait ) {
                    $libelle = $FraisForfait['libelle'];
        ?>	
                    <th> <?php echo $libelle?></th>
        <?php
                }
        ?>
            </tr>
            <tr>
                <th>Quantite</th>
        <?php
                foreach (  $lesFraisForfait as $FraisForfait  ) {
                    $quantiteForfait = $FraisForfait['quantite'];
	?>
                    <td class="qteForfait"><?php echo $quantiteForfait;?> </td>
	<?php
                }
	?>
            </tr>
            <tr>
                <th>Prix total</th>
                <?php
                $coutTotalFrais = 0;
                foreach (  $lesFraisForfait as $FraisForfait  ) {
                    $coutTotalFrais += $FraisForfait['quantite'] * $FraisForfait['montant'];
                    $coutFraisForfait = $FraisForfait['quantite'] * $FraisForfait['montant'];
                    ?>
                    <td class="qteForfait"><?php echo $coutFraisForfait." €";?> </td>
                    <?php
                }
                ?>
            </tr>
        </table></br>

        <!-- formulaire pour modification des elements -->
        <form action="index.php?uc=validerFrais&action=modifier" method="POST">
            <input type="hidden" name="idVisiteur" value="<?php echo $visiteurASelectionner ?>" />
            <input type="hidden" name="unMois" value="<?php echo $moisASelectionner ?>" />
            <input type="submit" value="Modifier" />
            
        </form>

        <!-- tableau des frais hors forfait -->
  	<table class="listeLegere">
            <caption class="caption">Descriptif des éléments hors forfait -<?php echo $nbJustificatifs ?> justificatifs reçus -</caption>
            <tr>
                <th class="date">Date</th>
                <th class="libelle">Libellé</th>
                <th class='montant'>Montant</th>
                <th>Reporter frais</th> 
                <th>Refuser frais</th>
            </tr>
        <!-- boucle foreach du tableau-->
        <?php
            $coutFraisHorsForfait = 0;
            foreach ( $lesFraisHorsForfait as $FraisHorsForfait ) 
            {
                    $idFrais = $FraisHorsForfait['id'];
                    $date = $FraisHorsForfait['date'];
                    $libelle = $FraisHorsForfait['libelle'];
                    $montant = $FraisHorsForfait['montant'];
                    $testRefus = substr($libelle, 0, 5);
                if ($testRefus !== "REFUS") {
                        $coutFraisHorsForfait += $FraisHorsForfait['montant'];
                    }
          ?>
                <tr>
                <td><?php echo $date ?></td>
                <td><?php echo $libelle ?></td>
                <td><?php echo $montant ?> €</td>
                
                <!-- reporter -->
                <form action="index.php?uc=validerFrais&action=reporter" method="POST">
                    <input type="hidden" name="idFraisHorsForfait" value="<?php echo $idFrais ?>" />
                    <input type="hidden" name="idVisiteur" value="<?php echo $visiteurASelectionner ?>" />
                    <input type="hidden" name="unMois" value="<?php echo $moisASelectionner ?>" />
                    <td><input type="submit" value="Reporter" /></td>
                </form>

                <!-- refuser -->
                <form action="index.php?uc=validerFrais&action=refuser" method="POST">
                    <input type="hidden" name="idFraisHorsForfait" value="<?php echo $idFrais ?>" />
                    <input type="hidden" name="idVisiteur" value="<?php echo $visiteurASelectionner ?>" />
                    <input type="hidden" name="unMois" value="<?php echo $moisASelectionner ?>" />
                    <td><input type="submit" value="Refuser" /></td>
                </form>
                </tr>
            <?php
            }
	?>
                <tr>
                    <th colspan="2">Prix total</th>
                    <td colspan="3"><?php echo $coutFraisHorsForfait; ?> €</td>
                </tr>
        </table>
    <p class="p"> Total à payer : <?php echo $coutFraisHorsForfait + $coutTotalFrais; ?> €</p>
        <form action="index.php?uc=validerFrais&action=validerFicheFrais" method="post">
            <input type="hidden" name="idVisiteur" value="<?php echo $visiteurASelectionner ?>" />
            <input type="hidden" name="unMois" value="<?php echo $moisASelectionner ?>" />
            <input type="hidden" name="montantValide" value="<?php echo $coutFraisHorsForfait + $coutTotalFrais ?>" />
            <input type="submit" value="Valider la fiche" />
        </form> 
    </div>
</div>