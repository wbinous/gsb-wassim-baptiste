﻿<?php
/** 
 * Classe d'accès aux données. 
 
 * Utilise les services de la classe PDO
 * pour l'application GSB
 * Les attributs sont tous statiques,
 * les 4 premiers pour la connexion
 * $monPdo de type PDO 
 * $monPdoGsb qui contiendra l'unique instance de la classe
 
 * @package default
 * @author Cheri Bibi
 * @version    1.0
 * @link       http://www.php.net/manual/fr/book.pdo.php
 */

class PdoGsb{   		
      	private static $serveur='mysql:host=localhost';
      	private static $bdd='dbname=gsb_frais';
      	private static $user='root';
      	private static $mdp='';
		private static $monPdo;
		private static $monPdoGsb=null;
/**
 * Constructeur privé, crée l'instance de PDO qui sera sollicitée
 * pour toutes les méthodes de la classe
 */				
	private function __construct(){
    	PdoGsb::$monPdo = new PDO(PdoGsb::$serveur.';'.PdoGsb::$bdd, PdoGsb::$user, PdoGsb::$mdp); 
		PdoGsb::$monPdo->query("SET CHARACTER SET utf8");
	}
	public function _destruct(){
		PdoGsb::$monPdo = null;
	}
/**
 * Fonction statique qui crée l'unique instance de la classe
 
 * Appel : $instancePdoGsb = PdoGsb::getPdoGsb();
 
 * @return l'unique objet de la classe PdoGsb
 */
	public  static function getPdoGsb(){
		if(PdoGsb::$monPdoGsb==null){
			PdoGsb::$monPdoGsb= new PdoGsb();
		}
		return PdoGsb::$monPdoGsb;  
	}
/**
 * Retourne les informations d'un visiteur
 
 * @param $login 
 * @param $mdp
 * @return l'id, le nom et le prénom sous la forme d'un tableau associatif 
*/
	public function getInfosVisiteur($login, $mdp, $type){
		$req = "select visiteur.idVisiteur as id, visiteur.nom as nom, visiteur.prenom as prenom from visiteur 
		where visiteur.login='$login' and visiteur.mdp=md5('$mdp') and visiteur.comptable='$type'";
		$rs = PdoGsb::$monPdo->query($req);
		$ligne = $rs->fetch();
		return $ligne;
	}

    public function getLesVisiteurs($mois){
        $req = "SELECT visiteur.idVisiteur ,visiteur.nom, visiteur.prenom, fichefrais.mois FROM fichefrais NATURAL JOIN visiteur WHERE fichefrais.idVisiteur = visiteur.idVisiteur AND fichefrais.idEtat = 'CL' AND mois='$mois' AND visiteur.comptable = 0;";
        $res = PdoGsb::$monPdo->query($req);
        $lesVisiteurs =array();
        $laLigne = $res->fetch();
        while($laLigne != null)	{
            $id = $laLigne['idVisiteur'];
            $nom = $laLigne['nom'];
            $prenom = $laLigne['prenom'];
            $mois = $laLigne['mois'];
            $lesVisiteurs["$nom"]=array(
                "id"=>"$id",
                "nom"=>"$nom",
                "prenom"  => "$prenom",
                "mois" => "$mois",

            );
            $laLigne = $res->fetch();
        }

       // var_dump($lesVisiteurs) ;
        return $lesVisiteurs;
    }

/**
 * Retourne sous forme d'un tableau associatif toutes les lignes de frais hors forfait
 * concernées par les deux arguments
 
 * La boucle foreach ne peut être utilisée ici car on procède
 * à une modification de la structure itérée - transformation du champ date-
 
 * @param $idVisiteur 
 * @param $mois sous la forme aaaamm
 * @return tous les champs des lignes de frais hors forfait sous la forme d'un tableau associatif 
*/
	public function getLesFraisHorsForfait($idVisiteur,$mois){
        $lesLignes = 0;
	    $req = "select * from lignefraishorsforfait where lignefraishorsforfait.idvisiteur ='$idVisiteur' 
		and lignefraishorsforfait.mois = '$mois';";
        $res = PdoGsb::$monPdo->query($req);
        if (!empty($res)) {
            $lesLignes = $res->fetchAll();
            $nbLignes = count($lesLignes);
            for ($i=0; $i<$nbLignes; $i++){
                $date = $lesLignes[$i]['date'];
                $lesLignes[$i]['date'] =  dateAnglaisVersFrancais($date);
            }
        }
		return $lesLignes; 
	}
/**
 * Retourne le nombre de justificatif d'un visiteur pour un mois donné
 
 * @param $idVisiteur 
 * @param $mois sous la forme aaaamm
 * @return le nombre entier de justificatifs 
*/
	public function getNbjustificatifs($idVisiteur, $mois){
		$req = "select fichefrais.nbjustificatifs as nb from  fichefrais where fichefrais.idvisiteur ='$idVisiteur' and fichefrais.mois = '$mois'";
		$res = PdoGsb::$monPdo->query($req);
		$laLigne = $res->fetch();
		return $laLigne['nb'];
	}
/**
 * Retourne sous forme d'un tableau associatif toutes les lignes de frais au forfait
 * concernées par les deux arguments
 
 * @param $idVisiteur 
 * @param $mois sous la forme aaaamm
 * @return l'id, le libelle et la quantité sous la forme d'un tableau associatif 
*/
	public function getLesFraisForfait($idVisiteur, $mois){
		// Code à ajouter
        $req = "SELECT fraisforfait.id, fraisforfait.libelle, fraisforfait.montant, lignefraisforfait.idVisiteur, lignefraisforfait.mois, lignefraisforfait.idFraisForfait, lignefraisforfait.quantite
                FROM lignefraisforfait JOIN fraisforfait ON lignefraisforfait.idFraisForfait = fraisforfait.id WHERE lignefraisforfait.idVisiteur = '$idVisiteur' AND lignefraisforfait.mois= '$mois';";
        $res = PdoGsb::$monPdo->query($req);
        $lesLignes = $res->fetchAll();
        return $lesLignes;
    }
/**
 * Retourne tous les id de la table FraisForfait
 
 * @return un tableau associatif 
*/
	public function getLesIdFrais(){
		// Code à ajouter
        $req = "SELECT id FROM fraisforfait;";
        $res = PdoGsb::$monPdo->query($req);
        $lesLignes = $res->fetchAll();
		return $lesLignes;
	}
/**
 * Met à jour la table ligneFraisForfait
 
 * Met à jour la table ligneFraisForfait pour un visiteur et
 * un mois donné en enregistrant les nouveaux montants
 
 * @param $idVisiteur 
 * @param $mois sous la forme aaaamm
 * @param $lesFrais tableau associatif de clé idFrais et de valeur la quantité pour ce frais
 * @return un tableau associatif 
*/
	public function majFraisForfait($idVisiteur, $mois, $lesFrais){
		$lesCles = array_keys($lesFrais);
		foreach($lesCles as $unIdFrais){
			$qte = $lesFrais[$unIdFrais];
			$req = "update lignefraisforfait set lignefraisforfait.quantite = $qte
			where lignefraisforfait.idvisiteur = '$idVisiteur' and lignefraisforfait.mois = '$mois'
			and lignefraisforfait.idfraisforfait = '$unIdFrais'";
			PdoGsb::$monPdo->exec($req);
		}
	}
/**
 * met à jour le nombre de justificatifs de la table ficheFrais
 * pour le mois et le visiteur concerné
 
 * @param $idVisiteur 
 * @param $mois sous la forme aaaamm
*/
	public function majNbJustificatifs($idVisiteur, $mois, $nbJustificatifs){
		// Code à ajouter
        $req = "UPDATE fichefrais SET nbJustificatifs = '$nbJustificatifs' WHERE idVisiteur = '$idVisiteur' AND fichefrais.mois = '$mois'";
        PdoGsb::$monPdo->exec($req);
	}
/**
 * Teste si un visiteur possède une fiche de frais pour le mois passé en argument
 
 * @param $idVisiteur 
 * @param $mois sous la forme aaaamm
 * @return vrai ou faux 
*/	
	public function estPremierFraisMois($idVisiteur,$mois)
	{
		// Code à ajouter
        $possedeFiche = false;

        $req = "SELECT COUNT(*) as nbRep FROM fichefrais WHERE idVisiteur= '$idVisiteur' AND mois = '$mois';";
        $res = PdoGsb::$monPdo->query($req);
        $lesLignes = $res->fetchAll();
        $rowcount=count($lesLignes);
        if($rowcount == 0){
            return $possedeFiche;
        }else{
            $possedeFiche = true;
        }
        return $possedeFiche;
	}
/**
 * Retourne le dernier mois en cours d'un visiteur
 
 * @param $idVisiteur 
 * @return le mois sous la forme aaaamm
*/	
	public function dernierMoisSaisi($idVisiteur){
		// Code à ajouter
        $req = "SELECT MAX(fichefrais.mois) as mois FROM fichefrais WHERE idEtat = 'CL' AND idVisiteur = '$idVisiteur';";
        $res = PdoGsb::$monPdo->query($req);
        $laLigne = $res->fetch();
        return $laLigne['mois'];
	}
	
/**
 * Crée une nouvelle fiche de frais et les lignes de frais au forfait pour un visiteur et un mois donnés
 
 * récupère le dernier mois en cours de traitement, met à 'CL' son champs idEtat, crée une nouvelle fiche de frais
 * avec un idEtat à 'CR' et crée les lignes de frais forfait de quantités nulles 
 * @param $idVisiteur 
 * @param $mois sous la forme aaaamm
*/
	public function creeNouvellesLignesFrais($idVisiteur, $mois){
		// Code à ajouter
        $dernierMois = $this->dernierMoisSaisi($idVisiteur);
        $laDerniereFiche = $this->getLesInfosFicheFrais($idVisiteur,$dernierMois);
        if($laDerniereFiche['idEtat'] == 'CR'){
            $this->majEtatFicheFrais($idVisiteur, $dernierMois,'CL');
        }

        $req = "INSERT INTO fichefrais(idVisiteur,mois,nbJustificatifs,montantValide,dateModif,idEtat) 
		VALUES('$idVisiteur','$mois',0,0,now(),'CR')";
        PdoGsb::$monPdo->exec($req);
        $lesFrais = $this->getLesIdFrais();
        foreach($lesFrais as $uneLigneFrais){
            $unFrais = $uneLigneFrais['id'];
            $req = "INSERT INTO lignefraisforfait(idVisiteur,mois,idFraisForfait,quantite) 
			VALUES('$idVisiteur','$mois','$unFrais',0)";
            PdoGsb::$monPdo->exec($req);
        }
	}
/**
 * Crée un nouveau frais hors forfait pour un visiteur un mois donné
 * à partir des informations fournies en paramètre
 
 * @param $idVisiteur 
 * @param $mois sous la forme aaaamm
 * @param $libelle : le libelle du frais
 * @param $date : la date du frais au format français jj//mm/aaaa
 * @param $montant : le montant
*/
	public function creeNouveauFraisHorsForfait($idVisiteur,$mois,$libelle,$date,$montant){
		// Code à ajouter
        $req = "INSERT INTO fraishorforfait (idVisiteur,mois,libelle,date,montant) VALUES ('$idVisiteur','$mois','$libelle','$date','$montant')";
        PdoGsb::$monPdo->exec($req);

	}
/**
 * Supprime le frais hors forfait dont l'id est passé en argument
 
 * @param $idFrais 
*/
	public function supprimerFraisHorsForfait($idFrais){
		// Code à ajouter
        $req = "DELETE FROM lignefraishorsforfait WHERE id = '$idFrais';";
        PdoGsb::$monPdo->exec($req);
	}

    public function refuserFraisHorsForfait($idFrais){
        // Code à ajouter
        $req = "UPDATE lignefraishorsforfait SET libelle = CONCAT('REFUSE : ', libelle) WHERE id = '$idFrais';";
        PdoGsb::$monPdo->exec($req);
    }
/**
 * Retourne les mois pour lesquel un visiteur a une fiche de frais
 
 * @param $idVisiteur 
 * @return un tableau associatif de clé un mois -aaaamm- et de valeurs l'année et le mois correspondant 
*/
	public function getLesMoisDisponibles($idVisiteur){
		$req = "select fichefrais.mois as mois from  fichefrais where fichefrais.idvisiteur ='$idVisiteur' 
		order by fichefrais.mois desc ";
		$res = PdoGsb::$monPdo->query($req);
		$lesMois =array();
		$laLigne = $res->fetch();
		while($laLigne != NULL)	{
			$mois = $laLigne['mois'];
            $mois = $laLigne['mois'];
            $numAnnee =substr( $mois,0,4);
			$numMois =substr( $mois,4,2);
			$lesMois["$mois"]=array(
				"mois"=>"$mois",
		    	"numAnnee"  => "$numAnnee",
				"numMois"  => "$numMois"
             );
			$laLigne = $res->fetch(); 		
		}
		return $lesMois;
	}
/**
 * Retourne les informations d'une fiche de frais d'un visiteur pour un mois donné
 
 * @param $idVisiteur 
 * @param $mois sous la forme aaaamm
 * @return un tableau avec des champs de jointure entre une fiche de frais et la ligne d'état 
*/	
	public function getLesInfosFicheFrais($idVisiteur,$mois){
		// Code à ajouter
        $req = "SELECT idVisiteur, nom, prenom, idEtat, mois, montantValide, nbJustificatifs, dateModif, libelle as libEtat FROM fichefrais JOIN etat ON etat.id = fichefrais.idEtat NATURAL JOIN visiteur WHERE idVisiteur = '$idVisiteur' AND mois = '$mois';";
        $res = PdoGsb::$monPdo->query($req);
        $lesLignes = $res->fetch();
        return $lesLignes;
	}
/**
 * Modifie l'état et la date de modification d'une fiche de frais
 
 * Modifie le champ idEtat et met la date de modif à aujourd'hui
 * @param $idVisiteur 
 * @param $mois sous la forme aaaamm
 */
 
	public function majEtatFicheFrais($idVisiteur,$mois,$etat){
		// Code à ajouter
        $req = "UPDATE fichefrais SET idEtat = '$etat' WHERE idVisiteur = '$idVisiteur' AND mois = '$mois';
                UPDATE fichefrais SET dateModif = NOW() WHERE idVisiteur = '$idVisiteur' AND mois = '$mois';";
        PdoGsb::$monPdo->exec($req);
	}

    function reporterFraisHorsForfait($idFrais, $mois) {
        $req = "UPDATE lignefraishorsforfait SET mois = '$mois' WHERE id = '$idFrais'";
        PdoGsb::$monPdo->exec($req);
    }

    /**
     * Recupère les mois non validés
     */
    public function getLesMoisEnAttente(){
        $req = "SELECT mois FROM fichefrais NATURAL JOIN visiteur WHERE idEtat = 'CL' AND comptable = 0 ORDER BY mois DESC;";
        $res = PdoGsb::$monPdo->query($req);
        $lesMois =array();
        $laLigne = $res->fetch();
        while($laLigne != NULL)	{
            $mois = $laLigne['mois'];
            $numAnnee =substr( $mois,0,4);
            $numMois =substr( $mois,4,2);
            $lesMois["$mois"]=array(
                "mois"=>"$mois",
                "numAnnee"  => "$numAnnee",
                "numMois"  => "$numMois"
            );
            $laLigne = $res->fetch();
        }
        return $lesMois;
    }

    public function majMontantValide($idVisiteur, $mois, $montant) {
        $req = "UPDATE fichefrais SET montantValide = '$montant' WHERE idVisiteur = '$idVisiteur' AND mois = '$mois';";
        PdoGsb::$monPdo->exec($req);
    }

    /*public function calculerFraisForfait($idVisiteur, $mois){
        $req = "SELECT fraisforfait.id, fraisforfait.libelle, fraisforfait.montant, lignefraisforfait.idVisiteur, lignefraisforfait.mois, lignefraisforfait.idFraisForfait, lignefraisforfait.quantite
                FROM lignefraisforfait JOIN fraisforfait ON lignefraisforfait.idFraisForfait = fraisforfait.id WHERE lignefraisforfait.idVisiteur = '$idVisiteur' AND lignefraisforfait.mois= '$mois';";
        $res = PdoGsb::$monPdo->query($req);
        $lesLignes = $res->fetchAll();
       // $coutTotal = $LesLignes['montant'];
        $coutTotal = $lesLignes[0]['quantite'];
        /* foreach($lesLignes as $uneLigneFrais){
            $coutTotal += $uneLigneFrais['montant'] * $uneLigneFrais['quantite'];
        }
        return $coutTotal;
    }*/
    public function getFichesFraisValidees(){
        $req = "SELECT fichefrais.id, fichefrais.idVisiteur, nom, prenom, idEtat, mois, montantValide, nbJustificatifs, dateModif, libelle as libEtat FROM fichefrais JOIN etat ON etat.id = fichefrais.idEtat JOIN visiteur ON visiteur.idVisiteur = fichefrais.idVisiteur WHERE idEtat='VA';";
        $res = PdoGsb::$monPdo->query($req);
        $lesLignes = $res->fetchAll();
        return $lesLignes;
    }
}
?>