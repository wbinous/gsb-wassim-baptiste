﻿<?php
/** 
 * Fonctions pour l'application GSB
 
 * @package default
 * @author Cheri Bibi
 * @version    1.0
 */
 /**
 * Teste si un quelconque visiteur est connecté
 * @return vrai ou faux 
 */
function estConnecte(){
  return isset($_SESSION['idVisiteur']);
}
/**
 * Enregistre dans une variable session les infos d'un visiteur
 
 * @param $id 
 * @param $nom
 * @param $prenom
 */
function connecter($id,$nom,$prenom, $type){
	$_SESSION['idVisiteur']= $id; 
	$_SESSION['nom']= $nom;
	$_SESSION['prenom']= $prenom;
    $_SESSION['idType'] = $type;
    if($type == 0) {
	    $_SESSION['type']="Visiteur";
    } else {
	    $_SESSION['type']="Comptable";
    }
}
/**
 * Détruit la session active
 */
function deconnecter(){
	session_destroy();
}
/**
 * Transforme une date au format français jj/mm/aaaa vers le format anglais aaaa-mm-jj
 
 * @param $madate au format  jj/mm/aaaa
 * @return la date au format anglais aaaa-mm-jj
*/
function dateFrancaisVersAnglais($maDate){
	@list($jour,$mois,$annee) = explode('/',$maDate);
	return date('Y-m-d',mktime(0,0,0,$mois,$jour,$annee));
}
/**
 * Transforme une date au format format anglais aaaa-mm-jj vers le format français jj/mm/aaaa 
 
 * @param $madate au format  aaaa-mm-jj
 * @return la date au format format français jj/mm/aaaa
*/
function dateAnglaisVersFrancais($maDate){
   @list($annee,$mois,$jour)=explode('-',$maDate);
   $date="$jour"."/".$mois."/".$annee;
   return $date;
}
/**
 * retourne le mois au format aaaamm selon le jour dans le mois
 
 * @param $date au format  jj/mm/aaaa
 * @return le mois au format aaaamm
*/
function getMois($date){
		@list($jour,$mois,$annee) = explode('/',$date);
		if(strlen($mois) == 1){
			$mois = "0".$mois;
		}
		return $annee.$mois;
}

/* gestion des erreurs*/
/**
 * Indique si une valeur est un entier positif ou nul
 
 * @param $valeur
 * @return vrai ou faux
*/
function estEntierPositif($valeur) {
	return preg_match("/[^0-9]/", $valeur) == 0;
	
}

/**
 * Indique si un tableau de valeurs est constitué d'entiers positifs ou nuls
 
 * @param $tabEntiers : le tableau
 * @return vrai ou faux
*/
function estTableauEntiers($tabEntiers) {
	$ok = true;
	foreach($tabEntiers as $unEntier){
		if(!estEntierPositif($unEntier)){
		 	$ok=false; 
		}
	}
	return $ok;
}
/**
 * Vérifie si une date est inférieure d'un an à la date actuelle
 
 * @param $dateTestee 
 * @return vrai ou faux
*/
function estDateDepassee($dateTestee){
	$dateActuelle=date("d/m/Y");
	@list($jour,$mois,$annee) = explode('/',$dateActuelle);
	$annee--;
	$AnPasse = $annee.$mois.$jour;
	@list($jourTeste,$moisTeste,$anneeTeste) = explode('/',$dateTestee);
	return ($anneeTeste.$moisTeste.$jourTeste < $AnPasse); 
}
/**
 * Vérifie la validité du format d'une date française jj/mm/aaaa 
 
 * @param $date 
 * @return vrai ou faux
*/
function estDateValide($date){
	$tabDate = explode('/',$date);
	$dateOK = true;
	if (count($tabDate) != 3) {
	    $dateOK = false;
    }
    else {
		if (!estTableauEntiers($tabDate)) {
			$dateOK = false;
		}
		else {
			if (!checkdate($tabDate[1], $tabDate[0], $tabDate[2])) {
				$dateOK = false;
			}
		}
    }
	return $dateOK;
}

/**
 * Vérifie que le tableau de frais ne contient que des valeurs numériques 
 
 * @param $lesFrais 
 * @return vrai ou faux
*/
function lesQteFraisValides($lesFrais){
	return estTableauEntiers($lesFrais);
}
/**
 * Vérifie la validité des trois arguments : la date, le libellé du frais et le montant 
 
 * des message d'erreurs sont ajoutés au tableau des erreurs
 
 * @param $dateFrais 
 * @param $libelle 
 * @param $montant
 */
function valideInfosFrais($dateFrais,$libelle,$montant){
	if($dateFrais==""){
		ajouterErreur("Le champ date ne doit pas être vide");
	}
	else{
		if(!estDatevalide($dateFrais)){
			ajouterErreur("Date invalide");
		}	
		else{
			if(estDateDepassee($dateFrais)){
				ajouterErreur("date d'enregistrement du frais dépassé, plus de 1 an");
			}			
		}
	}
	if($libelle == ""){
		ajouterErreur("Le champ description ne peut pas être vide");
	}
	if($montant == ""){
		ajouterErreur("Le champ montant ne peut pas être vide");
	}
	else
		if( !is_numeric($montant) ){
			ajouterErreur("Le champ montant doit être numérique");
		}
}
/**
 * Ajoute le libellé d'une erreur au tableau des erreurs 
 
 * @param $msg : le libellé de l'erreur 
 */
function ajouterErreur($msg){
   if (! isset($_REQUEST['erreurs'])){
      $_REQUEST['erreurs']=array();
	} 
   $_REQUEST['erreurs'][]=$msg;
}
/**
 * Retoune le nombre de lignes du tableau des erreurs 
 
 * @return le nombre d'erreurs
 */
function nbErreurs(){
   if (!isset($_REQUEST['erreurs'])){
	   return 0;
	}
	else{
	   return count($_REQUEST['erreurs']);
	}
}

function incrementerMois($date) {
    $numAnnee = substr($date, 0, 4);
    $numMois = substr($date, 4, 2);

    if ($numMois > 11) {
        $numAnnee = $numAnnee + 1;
        $numMois = "01";
    } else {
        $numMois += 1;
    }

    return (string)$numAnnee.(string)$numMois;
}

function moisAnglaisVersFrancais($mois) {
    $numMois = substr($mois, 4, 2);
    $intituleMois = array('Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre');
    return $intituleMois[$numMois - 1];
}

function creerPdfInscription($uneFiche, $lesFraisForfait, $lesFraisHorsForfait, $prixRefuse) {
    // permet d'inclure la bibliothèque fpdf
    require('fpdf/fpdf.php');
    // instancie un objet de type FPDF qui permet de créer le PDF
    ob_end_clean(); //    the buffer and never prints or returns anything.
    ob_start(); // it starts buffering
    $pdf=new FPDF();
    // ajoute une page
    $pdf->AddPage();
    // définit la police courante
    $pdf->SetFont('Arial','B',16);
    // Ajoute une image
    $pdf->Image('./images/logo.jpg',10,10, 64, 48);
    // affiche du texte encore
    $pdf->Cell(45,30,'', '',1);
    $pdf->Cell(10,10,'REMBOURSEMENT DE FRAIS ENGAGES');
    $pdf->Cell(60,35,'', '',1);
    $pdf->SetFont('Arial','B',12);
    $pdf->Cell(60,5,'Visiteur : '. $uneFiche['nom']. ' '. $uneFiche['prenom'], '', 1);
    $pdf->Cell(60,1,'', '',1);
    $pdf->Cell(60,10,'Mois : '. $uneFiche['mois'], '', 1);
    $pdf->SetFont('Arial', '',11);
    $pdf->Cell(40,5,'Frais forfaitaires', 'LTRB', 0);
    $pdf->Cell(40,5,utf8_decode('Quantité'), 'LTRB', 0);
    $pdf->Cell(40,5,'Montant Unitaire', 'LTRB', 0);
    $pdf->Cell(40,5,'Total', 'LTRB', 1);
    foreach($lesFraisForfait as $unFraisForfait) {
        $pdf->Cell(40,5,utf8_decode($unFraisForfait['libelle']), 'LTRB', 0);
        $pdf->Cell(40,5,$unFraisForfait['quantite']. ' '. chr(128), 'LTRB', 0);
        $pdf->Cell(40,5,$unFraisForfait['montant']. ' '. chr(128), 'LTRB', 0);
        $pdf->Cell(40,5,$unFraisForfait['quantite'] * $unFraisForfait['montant']. ' '.chr(128), 'LTRB', 1);
    }
    $pdf->Cell(60,5,'', '', 1);
    $pdf->SetFont('Arial','B',16);
    $pdf->Cell(60,5,'Autres frais', '', 1);
    $pdf->Cell(60,2,'', '', 1);
    $pdf->SetFont('Arial', '',11);
    $pdf->Cell(40,5,'Date', 'LTRB', 0);
    $pdf->Cell(80,5,utf8_decode('Libellé'), 'LTRB', 0);
    $pdf->Cell(40,5,'Montant', 'LTRB', 1);
    foreach($lesFraisHorsForfait as $unFraisHorsForfait) {
        $pdf->Cell(40,5,utf8_decode($unFraisHorsForfait['date']), 'LTRB', 0);
        $pdf->Cell(80,5,$unFraisHorsForfait['libelle'], 'LTRB', 0);
        $pdf->Cell(40,5,$unFraisHorsForfait['montant']. ' '. chr(128), 'LTRB', 0);
    }
    $pdf->Cell(60,7,'', '', 1);
    $pdf->SetFont('Arial', '',12);
    $pdf->Cell(60,10,'TOTAL : '. $uneFiche['mois']);
    $pdf->Cell(60,10,$prixRefuse + $uneFiche['montantValide']. ' '. chr(128), '', 1);
    $pdf->Cell(60,15,utf8_decode('Montant refusé : '));
    $pdf->Cell(60,15,$prixRefuse. ' '. chr(128), '', 1);
    $pdf->Cell(60,15,utf8_decode('Montant validé : '));
    $pdf->Cell(60,15,$uneFiche['montantValide']. ' '. chr(128), '', 1);
    $pdf->Cell(40,15,utf8_decode('Fait à Paris le : '));
    $dateAjd = date('m/d/Y');
    $pdf->Cell(60,15,$dateAjd, '', 1);
    $pdf->Cell(60,15,"Vu l'agent comptable", '', 1);
    $pdf->SetFont('Arial','',12);
    // $pdf->Cell(10,10,'Client : '. $inscription['nom'].' '.$inscription['prenom'], 'L',1);
    $pdf->Cell(60,1,'', '',0);
    //  $pdf->Cell(10,10,'Cours numero : '. $inscription['numCours'], 'L',1);
    // Enfin, le document est terminé et envoyé au navigateur grâce à Output().
    $pdf->Output();
    ob_end_flush();
}
?>