<?php
if ($_SESSION['idtype'] == 1) {
    include("vues/v_sommaireComptable.php");
} else {
    include("vues/v_sommaire.php");
}
$idVisiteur = $_SESSION['idVisiteur'];
$mois = getMois(date("d/m/Y"));
$action = $_REQUEST['action'];
switch($action){

    case'suiviPaiement':{
        // affiche les fiches validées : etat='VA'
        $lesFiches=$pdo->getFichesFraisValidees();
        include ('vues/v_fichePayer.php');
        break;
    }

    case'suivreLePaiement':{
        $mois = null;
        $id = null;
        if(isset($_REQUEST['idVisiteur'])) {
            $lesidVisiteurs = $_REQUEST['idVisiteur'];
            foreach ($lesidVisiteurs as $idVisiteur) {
                $id = substr($idVisiteur, 0, strpos($idVisiteur, '-'));
                $mois = substr(strstr($idVisiteur, '-'), strlen('-'));
                $lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($id, $mois);
                $lesFraisForfait = $pdo->getLesFraisForfait($id, $mois);
                $uneFiche = $pdo->getLesInfosFicheFrais($id, $mois);
                $nom = $uneFiche['nom'];
                $prenom = $uneFiche['prenom'];
                $libEtat = $uneFiche['libEtat'];
                $montantValide = $uneFiche['montantValide'];
                $nbJustificatifs = $uneFiche['nbJustificatifs'];
                $dateModif = $uneFiche['dateModif'];
                $dateModif = dateAnglaisVersFrancais($dateModif);
                include("vues/v_suivrePaiement.php");
            }
        } else {
            echo "Aucune fiche selectionnée.";
        }
        break;
    }

    case'miseEnPaiement':{
        $leVisiteur=$_REQUEST['idV'];
        $leMois=$_REQUEST['mois'];
        $pdo->majEtatFicheFrais($leVisiteur,$leMois,'RB');
        include("vues/v_rembourser.php");
        break;
    }

    case 'genererPDF':{
        $idVisiteur = $_REQUEST['i'];
        $mois = $_REQUEST['m'];
        $prixRefuse = 0;
        $lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($idVisiteur, $mois);
        $lesFraisForfait= $pdo->getLesFraisForfait($idVisiteur, $mois);
        $uneFiche = $pdo->getLesInfosFicheFrais($idVisiteur, $mois);
        foreach ( $lesFraisHorsForfait as $FraisHorsForfait ) {
            $libelle = $FraisHorsForfait['libelle'];
            $montant = $FraisHorsForfait['montant'];
            $testRefus = substr($libelle, 0, 5);
            if ($testRefus == "REFUS") {
                $prixRefuse += $FraisHorsForfait['montant'];
            }
        }
        include('vues/v_consultationPDF.php');
        break;
    }
}
?>