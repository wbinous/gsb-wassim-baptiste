﻿<?php
if(!isset($_REQUEST['action'])){
	$_REQUEST['action'] = 'demandeConnexion';
}
$action = $_REQUEST['action'];
switch($action){
	case 'demandeConnexion':{
		include("vues/v_connexion.php");
		break;
	}
	case 'valideConnexion':{
		$login = $_REQUEST['login'];
		$mdp = $_REQUEST['mdp'];
		$type = $_REQUEST['type'];
        $_SESSION['idtype'] = $type;
		$visiteur = $pdo->getInfosVisiteur($login,$mdp, $type);
		if(!is_array($visiteur)){
			ajouterErreur("Login ou mot de passe incorrect");
			include("vues/v_erreurs.php");
			include("vues/v_connexion.php");
		}
		else{
			$id = $visiteur['id'];
            $idVisiteur = $visiteur['id'];
			$nom =  $visiteur['nom'];
			$prenom = $visiteur['prenom'];
			connecter($id,$nom,$prenom, $type);
            if ($type == 1){
                include("vues/v_sommaireComptable.php");
                $lesMois = $pdo->getLesMoisDisponibles($idVisiteur);
                // Afin de sélectionner par défaut le dernier mois dans la zone de liste
                // on demande toutes les clés, et on prend la première,
                // les mois étant triés décroissants
                $lesCles = array_keys($lesMois);
                if (isset($lesCles[0])) {
                    $moisASelectionner = $lesCles[0];
                }
              //  include("vues/v_comptableIndex.php");
            }
            else{
                include("vues/v_sommaire.php");
            }
		}
		break;
	}
	default :{
		include("vues/v_connexion.php");
		break;
	}
}
?>