<?php
if ($_SESSION['idtype'] == 1) {
    include("vues/v_sommaireComptable.php");
} else {
    include("vues/v_sommaire.php");
}
$action = $_REQUEST['action'];
$idVisiteur = $_SESSION['idVisiteur'];
switch($action){
    case 'selectionnerMois':{
        $lesMois=$pdo->getLesMoisEnAttente();
        // Afin de sélectionner par défaut le dernier mois dans la zone de liste
        // on demande toutes les clés, et on prend la première,
        // les mois étant triés décroissants
        if(!empty($lesMois)) {
            $lesCles = array_keys($lesMois);
            $moisASelectionner = $lesCles[0];
            include("vues/v_listeMoisVisiteur.php");
            include("vues/v_listeVisiteur.php");
        } else {
            echo "Il n'y a pas de fiche disponible.";
        }
        break;
    }
    /* case 'selectionnerVisiteurs':{
        $moisASelectionner = $_REQUEST['lstMois'];
        $lesMois = $pdo->getLesMoisEnAttente();
        include("vues/v_listeMoisVisiteur.php");
        $lesVisiteurs=$pdo->getLesVisiteurs($moisASelectionner);
        include("vues/v_listeVisiteur.php");
        // Afin de sélectionner par défaut le dernier mois dans la zone de liste
        // on demande toutes les clés, et on prend la première,
        // les mois étant triés décroissants
        break;
    }*/
    case 'voirEtatFrais':{
        $idVisiteurMois = explode('/', $_REQUEST['lstVisiteurs']);
        $visiteurASelectionner = $idVisiteurMois[0];
        $moisASelectionner = $idVisiteurMois[1];
        $lesMois = $pdo->getLesMoisEnAttente();
        include("vues/v_listeMoisVisiteur.php");
        $lesVisiteurs = $pdo->getLesVisiteurs($moisASelectionner);
        include("vues/v_listeVisiteur.php");
        $lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($visiteurASelectionner,$moisASelectionner);
        $lesFraisForfait= $pdo->getLesFraisForfait($visiteurASelectionner,$moisASelectionner);
        $lesInfosFicheFrais = $pdo->getLesInfosFicheFrais($visiteurASelectionner,$moisASelectionner);
        // $coutTotalFraisForfait = $pdo->calculerFraisForfait($idVisiteur, $mois);
        $numAnnee =substr( $moisASelectionner,0,4);
        $numMois =substr( $moisASelectionner,4,2);
        $libEtat = $lesInfosFicheFrais['libEtat'];
        $montantValide = $lesInfosFicheFrais['montantValide'];
        $nbJustificatifs = $lesInfosFicheFrais['nbJustificatifs'];
        $dateModif = $lesInfosFicheFrais['dateModif'];
        $dateModif = dateAnglaisVersFrancais($dateModif);
        include("vues/v_etatFraisVisiteur.php");
        break;
    }
    case 'modifier':{
        $moisASelectionner = $_REQUEST['unMois'];
        $visiteurASelectionner = $_REQUEST['idVisiteur'];
        //recuperation du nombre de justificatifs
        $nbJustificatifs = $pdo->getNbJustificatifs($visiteurASelectionner,$moisASelectionner);
        //recuperation des frais forfait
        $lesFraisForfait = $pdo->getLesFraisForfait($visiteurASelectionner,$moisASelectionner);
        include('vues/v_modifier.php');
        break;
    }
    case 'validerModif':{
        $moisASelectionner = $_REQUEST['unMois'];
        $visiteurASelectionner = $_REQUEST['idVisiteur'];
        $lesFrais = $_REQUEST['lesFrais'];

        if(lesQteFraisValides($lesFrais)){
            $pdo->majFraisForfait($visiteurASelectionner,$moisASelectionner,$lesFrais);
        }
        include("vues/v_confirmModification.html");
        break;
    }
    case 'reporter':{
        $idFraisHorsForfait = $_REQUEST['idFraisHorsForfait'];
        $moisASelectionner = $_REQUEST['unMois'];
        $visiteurASelectionner = $_REQUEST['idVisiteur'];
        $numAnnee =substr( $moisASelectionner,0,4);
        $numMois =substr( $moisASelectionner,4,2);
        $mois = $pdo->dernierMoisSaisi($visiteurASelectionner);
        if($moisASelectionner == $mois) {
            $pdo->creeNouvellesLignesFrais($visiteurASelectionner, incrementerMois($moisASelectionner));
            $pdo->reporterFraisHorsForfait($idFraisHorsForfait, incrementerMois($mois));
        }
        else {
            $pdo->reporterFraisHorsForfait($idFraisHorsForfait, incrementerMois($moisASelectionner));
        }
        include("vues/v_confirmReport.html");
        break;
    }
    case 'refuser':{
        $idFraisHorsForfait = $_REQUEST['idFraisHorsForfait'];
        $pdo->refuserFraisHorsForfait($idFraisHorsForfait);
        include("vues/v_confirmRefuser.html");
        break;
    }

    case 'validerFicheFrais': {
        $moisASelectionner = $_REQUEST['unMois'];
        $visiteurASelectionner = $_REQUEST['idVisiteur'];
        $montantValide = $_REQUEST['montantValide'];
        $numAnnee = substr($moisASelectionner, 0, 4);
        $numMois = substr($moisASelectionner, 4, 2);
        $pdo->majEtatFicheFrais($visiteurASelectionner,$moisASelectionner,'VA');
        $pdo->majMontantValide($visiteurASelectionner, $moisASelectionner, $montantValide);
        include('vues/v_confirmValider.html');
        break;
    }
}
?>