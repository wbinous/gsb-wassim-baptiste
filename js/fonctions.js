$(function(){

    /*** Selection du mois ***/

    $("select[name='lstMois']").on("change",
    function() {

        $.get("js/retourJson.php",{"lstMois" : $(this).val() },
            foncRetour, "json");
    });

    function foncRetour(data) {

        $("#lstVisiteurs").empty();
        for (var key in data) {
            // alert(data[key].nom) ;
            $("#lstVisiteurs").append("<option value="+data[key].id+'/'+data[key].mois+">"+ data[key].nom +" "+ data[key].prenom+"</option>");
        }
    }

    /* $("select[name='lstMois']").on("change",
        function () {
            $("#listeVisiteurs").load("vues/v_listeVisiteur.php",{"lstMois" : $(this).val() });
        }); */

}); // fin fonction principale